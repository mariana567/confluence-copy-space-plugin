package com.atlassian.confluence.plugin.copyspace;

import com.atlassian.bandana.BandanaManager;
import com.atlassian.beehive.ClusterLockService;
import com.atlassian.confluence.audit.AuditingContext;
import com.atlassian.confluence.content.render.xhtml.links.LinksUpdater;
import com.atlassian.confluence.content.render.xhtml.migration.ExceptionTolerantMigrator;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.ContentPermissionManager;
import com.atlassian.confluence.core.ContentPropertyManager;
import com.atlassian.confluence.core.SynchronizationManager;
import com.atlassian.confluence.core.persistence.hibernate.HibernateSessionManager;
import com.atlassian.confluence.impl.security.query.SpacePermissionQueryManager;
import com.atlassian.confluence.internal.content.collab.ContentReconciliationManager;
import com.atlassian.confluence.internal.pages.persistence.AbstractPageDaoInternal;
import com.atlassian.confluence.internal.pages.persistence.PageDaoInternal;
import com.atlassian.confluence.internal.relations.RelationManager;
import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.links.LinkManager;
import com.atlassian.confluence.mail.notification.NotificationManager;
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.confluence.pages.CommentManager;
import com.atlassian.confluence.pages.DefaultPageManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.persistence.dao.BlogPostDao;
import com.atlassian.confluence.pages.templates.PageTemplateManager;
import com.atlassian.confluence.search.ChangeIndexer;
import com.atlassian.confluence.search.ConfluenceIndexer;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.security.SpacePermissionManager;
import com.atlassian.confluence.security.login.DefaultLoginManager;
import com.atlassian.confluence.security.login.LoginManager;
import com.atlassian.confluence.security.persistence.dao.UserLoginInfoDao;
import com.atlassian.confluence.setup.settings.CollaborativeEditingHelper;
import com.atlassian.confluence.setup.settings.DarkFeaturesManager;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceDescription;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.spaces.persistence.dao.SpaceDao;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.user.ConfluenceUserImpl;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.spring.container.ContainerContext;
import com.atlassian.spring.container.ContainerManager;
import com.atlassian.user.impl.DefaultUser;
import com.atlassian.vcache.VCacheFactory;
import com.google.common.base.Supplier;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.transaction.PlatformTransactionManager;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestSuppressAutowatch {

    // Mock objects
    @Mock
    private SettingsManager settingsManager;
    @Mock
    private UserAccessor userAccessor;
    @Mock
    private VCacheFactory cacheFactory;
    @Mock
    private UserLoginInfoDao loginInfoDao;
    @InjectMocks
    private DefaultLoginManager defaultLoginManager;

    @Mock
    private PageDaoInternal pageDao;
    @Mock
    private LinkManager linkManager;
    @Mock
    private ConfluenceIndexer indexer;
    @Mock
    private LabelManager labelManager;
    @Mock
    private AttachmentManager attachmentManager;
    @Mock
    private HibernateSessionManager hibernateSessionManager;
    @Mock
    private Supplier<XhtmlContent> xhtmlContentSupplier;
    @Mock
    private EventPublisher eventPublisher;
    @Mock
    private NotificationManager notificationManager;
    @Mock
    private BlogPostDao blogPostDao;
    @Mock
    private AbstractPageDaoInternal abstractPageDao;
    @Mock
    private SpaceDao spaceDao;
    @Mock
    private ContentPropertyManager contentPropertyManager;
    @Mock
    private Supplier<PermissionManager> permissionManagerSupplier;
    @Mock
    private ChangeIndexer changeIndexer;
    @Mock
    private LoginManager loginManager;
    @Mock
    private Supplier<ExceptionTolerantMigrator> revertedContentMigratorSupplier;
    @Mock
    private CollaborativeEditingHelper collaborativeEditingHelper;
    @Mock
    private RelationManager relationManager;
    @Mock
    private SpacePermissionQueryManager spacePermissionQueryManager;
    @Mock
    private ContentPermissionManager contentPermissionManager;
    @Mock
    private ClusterLockService clusterLockService;
    @Mock
    private LinksUpdater linksUpdater;
    @Mock
    private Supplier<UserAccessor> userAccessorSupplier;
    @Mock
    private SpacePermissionManager spacePermissionManager;
    @Mock
    private SynchronizationManager synchronizationManager;
    @Mock
    private PlatformTransactionManager transactionManager;
    @Mock
    private Supplier<DarkFeaturesManager> darkFeaturesManagerSupplier;
    @Mock
    private AuditingContext auditingContext;
    @InjectMocks
    private DefaultPageManager defaultPageManager;

    @Mock
    private SpaceManager spaceManager;
    @Mock
    private DefaultLabelCopier labelCopier;
    @Mock
    private PageTemplateManager pageTemplateManager;
    @Mock
    private CommentManager commentManager;
    @Mock
    private BandanaManager bandanaManager;
    @Mock
    private LookAndFeelCopier lookAndFeelCopier;
    @Mock
    private LogoCopier logoCopier;
    @Mock
    private AttachmentCopier attachmentCopier;
    @Mock
    private DecoratorCopier decoratorCopier;
    @Mock
    private SidebarLinkCopier sidebarLinkCopier;

    @Mock
    private ConfluenceUser user;
    @Mock
    private CopySpaceOptions options;

    private DefaultCopySpaceManager defaultCopySpaceManager;

    // Global test variables
    private Space originalSpace;
    private Space newSpace;

    private String newSpaceKey = "new";
    private String newSpaceName = "New Space";

    @Before
    public void setUp() throws NoSuchFieldException, IllegalAccessException {
        ContainerContext containerContext = mock(ContainerContext.class);
        ContainerManager.getInstance().setContainerContext(containerContext);
        ContentReconciliationManager contentReconciliationManager = mock(ContentReconciliationManager.class);
        when(containerContext.getComponent("contentReconciliationManager")).thenReturn(contentReconciliationManager);

        MockitoAnnotations.initMocks(this);

        Field userAccessorField = defaultPageManager.getClass().getSuperclass().getDeclaredField("userAccessor");
        userAccessorField.setAccessible(true);
        userAccessorField.set(defaultPageManager, userAccessorSupplier);
        userAccessorField.setAccessible(false);

        SpaceDescription spaceDescription = new SpaceDescription();
        String description = "This is description";
        spaceDescription.setBodyAsString(description);

        originalSpace = new Space();
        String originalSpaceKey = "tst";
        originalSpace.setKey(originalSpaceKey);
        String originalSpaceName = "Test";
        originalSpace.setName(originalSpaceName);
        originalSpace.setDescription(spaceDescription);

        newSpace = new Space();
        newSpace.setKey(newSpaceKey);
        newSpace.setName(newSpaceName);
        newSpace.setDescription(spaceDescription);

        user = new ConfluenceUserImpl(new DefaultUser("john.doe"));

        defaultCopySpaceManager = new DefaultCopySpaceManager(eventPublisher, indexer, contentPermissionManager,
                spaceManager, defaultPageManager, labelCopier, pageTemplateManager, commentManager, bandanaManager,
                lookAndFeelCopier, logoCopier, attachmentCopier, decoratorCopier, sidebarLinkCopier);

        when(spaceManager.createSpace(newSpaceKey, newSpaceName, originalSpace.getDescription().getBodyAsString(), user)).thenReturn(newSpace);
        when(options.isKeepMetaData()).thenReturn(false);
        when(options.isCopyComments()).thenReturn(true);
        when(options.isCopyAttachments()).thenReturn(true);
        when(options.isCopyPersonalLabels()).thenReturn(false);
        when(userAccessorSupplier.get()).thenReturn(userAccessor);
    }

    @Test
    public void testCopySpaceSuppressedAutowatch() throws CopySpaceException, IOException {
        Page page = new Page();
        page.setTitle("Page One");
        page.setBodyAsString("This is page one content");
        page.setPosition(1);
        page.setSpace(originalSpace);
        page.setCreator(user);
        List<Page> pages = Lists.newArrayList(page);

        when(defaultPageManager.getPages(originalSpace, true)).thenReturn(pages);

        doAnswer(invocation -> {
            Object[] args = invocation.getArguments();
            ((ContentEntityObject)args[0]).setCreator(user);
            return null;
        }).when(pageDao).save(any(Page.class));

        Space resultSpace = defaultCopySpaceManager.copySpace(originalSpace, newSpaceKey, newSpaceName, user, options);

        assertThat(resultSpace.getKey(), is(newSpaceKey));
        assertThat(resultSpace.getName(), is(newSpaceName));
        verify(pageDao, atLeastOnce()).save(any());
        verify(notificationManager, never()).addContentNotification(any(), any());
    }
}
