package com.atlassian.confluence.plugin.copyspace;

import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.plugins.ia.SidebarLink;
import com.atlassian.confluence.plugins.ia.SidebarLinkCategory;
import com.atlassian.confluence.plugins.ia.SidebarLinkManager;
import com.atlassian.confluence.plugins.ia.SidebarLinks;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceDescription;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestDefaultSidebarLinkCopier {

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private SidebarLinkManager sidebarLinkManager;
    @Captor
    private ArgumentCaptor<String> spaceKeyArgumentCaptor;
    @Captor
    private ArgumentCaptor<SidebarLinkCategory> categoryArgumentCaptor;
    @Captor
    private ArgumentCaptor<SidebarLink.Type> typeArgumentCaptor;
    @Captor
    private ArgumentCaptor<String> webItemKeyArgumentCaptor;
    @Captor
    private ArgumentCaptor<Integer> positionArgumentCaptor;
    @Captor
    private ArgumentCaptor<String> customTitleArgumentCaptor;
    @Captor
    private ArgumentCaptor<String> hardcodedUrlArgumentCaptor;
    @Captor
    private ArgumentCaptor<String> customIconClassArgumentCaptor;
    @Captor
    private ArgumentCaptor<Long> destinationPageIdArgumentCaptor;
    private DefaultSidebarLinkCopier sidebarLinkCopier;

    @Before
    public void setUp() {
        sidebarLinkCopier = new DefaultSidebarLinkCopier(sidebarLinkManager);
    }

    @Test
    public void testNoLinksCreatedWhenNoLinksInOriginalSpace() {
        final Space originalSpace = createSpace("ORIG", 1);
        final Space destinationSpace = createSpace("DEST", 2);

        when(sidebarLinkManager.findBySpace(eq(originalSpace.getKey()))).thenReturn(createLinks());
        sidebarLinkCopier.copySidebarLinks(originalSpace, destinationSpace, new SpaceCopyContext());
        verify(sidebarLinkManager, never()).createLink(anyString(), any(SidebarLinkCategory.class),
                any(SidebarLink.Type.class), anyString(), anyInt(), anyString(), anyString(), anyString(), anyLong());
    }

    @Test
    public void testPinnedPageLinkCreatedReplacingReference() {
        final Space originalSpace = createSpace("ORIG", 1);
        final Space destinationSpace = createSpace("DEST", 2);
        final Page originalPage = createPage(1);
        final Page destinationPage = createPage(2);
        final SpaceCopyContext spaceCopyContext = new SpaceCopyContext();
        spaceCopyContext.addPagePair(new SpaceCopyContext.ContentPair<>(originalPage, destinationPage));

        final SidebarLink sidebarLink = mock(SidebarLink.class);
        when(sidebarLink.getSpaceKey()).thenReturn(originalSpace.getKey());
        when(sidebarLink.getCategory()).thenReturn(SidebarLinkCategory.MAIN);
        when(sidebarLink.getType()).thenReturn(SidebarLink.Type.PINNED_PAGE);
        when(sidebarLink.getWebItemKey()).thenReturn("webitemkey");
        when(sidebarLink.getPosition()).thenReturn(0);
        when(sidebarLink.getCustomTitle()).thenReturn("customtitle");
        when(sidebarLink.getHardcodedUrl()).thenReturn("hardcodedurl");
        when(sidebarLink.getCustomIconClass()).thenReturn("customiconclass");
        when(sidebarLink.getDestPageId()).thenReturn(originalPage.getId());

        final SidebarLinks links = createLinks(sidebarLink);
        when(sidebarLinkManager.findBySpace(eq(originalSpace.getKey()))).thenReturn(links);

        sidebarLinkCopier.copySidebarLinks(originalSpace, destinationSpace, spaceCopyContext);
        verify(sidebarLinkManager).createLink(
                spaceKeyArgumentCaptor.capture(),
                categoryArgumentCaptor.capture(),
                typeArgumentCaptor.capture(),
                webItemKeyArgumentCaptor.capture(),
                positionArgumentCaptor.capture(),
                customTitleArgumentCaptor.capture(),
                hardcodedUrlArgumentCaptor.capture(),
                customIconClassArgumentCaptor.capture(),
                destinationPageIdArgumentCaptor.capture()
        );

        assertEquals(destinationSpace.getKey(), spaceKeyArgumentCaptor.getValue());
        assertEquals(SidebarLinkCategory.MAIN, categoryArgumentCaptor.getValue());
        assertEquals(SidebarLink.Type.PINNED_PAGE, typeArgumentCaptor.getValue());
        assertEquals("webitemkey", webItemKeyArgumentCaptor.getValue());
        assertEquals(0, positionArgumentCaptor.getValue().longValue());
        assertEquals("customtitle", customTitleArgumentCaptor.getValue());
        assertEquals("hardcodedurl", hardcodedUrlArgumentCaptor.getValue());
        assertEquals("customiconclass", customIconClassArgumentCaptor.getValue());
        assertEquals(spaceCopyContext.getCopiedPages().get(0).copy.getId(),
                destinationPageIdArgumentCaptor.getValue().longValue());
    }

    @Test
    public void testExternalLinkCreated() {
        final Space originalSpace = createSpace("ORIG", 1);
        final Space destinationSpace = createSpace("DEST", 2);
        final Page originalPage = createPage(1);
        final Page destinationPage = createPage(2);
        final SpaceCopyContext spaceCopyContext = new SpaceCopyContext();
        spaceCopyContext.addPagePair(new SpaceCopyContext.ContentPair<>(originalPage, destinationPage));

        final SidebarLink sidebarLink = mock(SidebarLink.class);
        when(sidebarLink.getSpaceKey()).thenReturn(originalSpace.getKey());
        when(sidebarLink.getCategory()).thenReturn(SidebarLinkCategory.MAIN);
        when(sidebarLink.getType()).thenReturn(SidebarLink.Type.EXTERNAL_LINK);
        when(sidebarLink.getWebItemKey()).thenReturn("webitemkey");
        when(sidebarLink.getPosition()).thenReturn(0);
        when(sidebarLink.getCustomTitle()).thenReturn("customtitle");
        when(sidebarLink.getHardcodedUrl()).thenReturn("hardcodedurl");
        when(sidebarLink.getCustomIconClass()).thenReturn("customiconclass");
        when(sidebarLink.getDestPageId()).thenReturn(originalPage.getId());

        final SidebarLinks links = createLinks(sidebarLink);
        when(sidebarLinkManager.findBySpace(eq(originalSpace.getKey()))).thenReturn(links);

        sidebarLinkCopier.copySidebarLinks(originalSpace, destinationSpace, spaceCopyContext);
        verify(sidebarLinkManager).createLink(
                spaceKeyArgumentCaptor.capture(),
                categoryArgumentCaptor.capture(),
                typeArgumentCaptor.capture(),
                webItemKeyArgumentCaptor.capture(),
                positionArgumentCaptor.capture(),
                customTitleArgumentCaptor.capture(),
                hardcodedUrlArgumentCaptor.capture(),
                customIconClassArgumentCaptor.capture(),
                destinationPageIdArgumentCaptor.capture()
        );

        assertEquals(destinationSpace.getKey(), spaceKeyArgumentCaptor.getValue());
        assertEquals(SidebarLinkCategory.MAIN, categoryArgumentCaptor.getValue());
        assertEquals(SidebarLink.Type.EXTERNAL_LINK, typeArgumentCaptor.getValue());
        assertEquals("webitemkey", webItemKeyArgumentCaptor.getValue());
        assertEquals(0, positionArgumentCaptor.getValue().longValue());
        assertEquals("customtitle", customTitleArgumentCaptor.getValue());
        assertEquals("hardcodedurl", hardcodedUrlArgumentCaptor.getValue());
        assertEquals("customiconclass", customIconClassArgumentCaptor.getValue());
        assertEquals(originalPage.getId(), destinationPageIdArgumentCaptor.getValue().longValue());
    }

    private Space createSpace(String key, int id) {
        Space space = new Space(key);
        space.setDescription(new SpaceDescription(space));
        space.setId(id);
        return space;
    }

    private Page createPage(int pageId) {
        final Page page = new Page();
        page.setId(pageId);
        page.setTitle("page " + pageId);
        return page;
    }

    private SidebarLinks createLinks(SidebarLink... links) {
        return new SidebarLinks(Arrays.asList(links));
    }
}
