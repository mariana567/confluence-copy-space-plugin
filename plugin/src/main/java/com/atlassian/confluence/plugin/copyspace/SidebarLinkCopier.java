package com.atlassian.confluence.plugin.copyspace;

import com.atlassian.confluence.spaces.Space;

/**
 * Responsible for copying sidebar links
 */
public interface SidebarLinkCopier {
    /**
     * Copies sidebar links from one space to another.
     *
     * @param originalSpace    from which to copy labels.
     * @param newSpace         to which the labels should be copied.
     * @param spaceCopyContext space copy context containing page & attachment pairs
     */
    void copySidebarLinks(Space originalSpace, Space newSpace, SpaceCopyContext spaceCopyContext);
}
